// The questions are as follows:
// What directive is used by Node.js in loading the modules it needs?
// What Node.js module contains a method for server creation?
// What is the method of the http object responsible for creating a server using Node.js?
// What method of the response object allows us to set status codes and content types?
// Where will console.log() output its contents when run in Node.js?
// What property of the request object contains the address' endpoint?

const HTTP = require(`http`);

let port = 3000;

HTTP.createServer((request, response) => {

    if (request.url === "/"){
        response.writeHead(200, {"Content-Type": `text/plain`});
        response.write(`This is the login page.`);
        response.end();
        
    } else {
        response.writeHead(400, {"Content-Type": `text/plain`});
        response.write(`An error occured.`);
        response.end();
    }

}).listen(port);